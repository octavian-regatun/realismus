﻿using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleInteraction : MonoBehaviour
{
    public GameObject vehicle;
    public float range, distance;
    public bool inRangeOfVehicle;

    private Vector3 playerPos;

    void Update()
    {
        distance = GetDistance();
        inRangeOfVehicle = CheckPlayerInRangeOfVehicle(range, distance);
    }

    float GetDistance()
    {
        Vector3 vehiclePos = vehicle.transform.position;

        playerPos = gameObject.transform.position;

        return Vector3.Distance(playerPos, vehiclePos);
    }

    bool CheckPlayerInRangeOfVehicle(float range, float distance)
    {
        if (distance <= range)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
